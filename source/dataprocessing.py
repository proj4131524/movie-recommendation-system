import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import NearestNeighbors

# Load movie data
def load_data(file_path):
        return pd.read_csv('/content/drive/MyDrive/content/movies-1.csv')
# Preprocess data
    def preprocess_data(data):

# Drop unnecessary columns
    data = data.drop(['budget'], axis=1)
# Remove any rows with missing values
     data = data.dropna()
     return data

# Split data into train and test sets
        def split_data(data):
        train, test = train_test_split(data, test_size=0.2, random_state=42)
        return train, test

# Train TF-IDF vectorizer
        def train_tfidf(train):
            tfidf_vectorizer = TfidfVectorizer(stop_words='english')
            tfidf_matrix = tfidf_vectorizer.fit_transform(train['genres'])
            return tfidf_vectorizer, tfidf_matrix

# Train nearest neighbors model
         def train_model(tfidf_matrix):
         nn_model = NearestNeighbors(metric='cosine', algorithm='brute')
         nn_model.fit(tfidf_matrix)
         return nn_model

# Generate movie recommendations
         def generate_recommendations(movie_title, tfidf_vectorizer, nn_model, train):
# Find index of movie in dataset
         movie_index = train[train['title'] == movie_title].index[0]
# Get TF-IDF vector for the given movie
        movie_tfidf = tfidf_vectorizer.transform([train.iloc[movie_index]['genres']])
# Find nearest neighbors
        distances, indices = nn_model.kneighbors(movie_tfidf, n_neighbors=5)
# Get recommendations
         recommendations = train.iloc[indices[0]]['title'].tolist()
            return recommendations

# Main function
     def main():
 # Load data
      movie_data = load_data('movies.csv')
# Preprocess data
     movie_data = preprocess_data(movie_data)
# Split data
 train_data, test_data = split_data(movie_data)
#Train TF-IDF vectorizer
tfidf_vectorizer, tfidf_matrix = train_tfidf(train_data)
# Train nearest neighbors model
  nn_model = train_model(tfidf_matrix)
  # Get movie recommendations
  movie_title = "user input"
  recommendations = generate_recommendations(movie_title, tfidf_vectorizer, nn_model, train_data)
  print(f"Recommendations for {movie_title}:")
     for movie in recommendations:
          print(movie)
     if _name_ == "_main_":
                main() :
